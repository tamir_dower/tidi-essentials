![Feaxes](feaxes.png)

This Minecraft Java Edition modification implements additional features and fixes various inconsistencies in the game.
Also adds configurable gameplay mechanics, decorative blocks and functional items.

In order to get the jar-file of the newest version designed for your mod loader and game version: import this project into your IDE, choose a correct branch and then execute the build task in the provided gradle wrapper.

**Due to limited amount of time and breaking changes in the game's code after each new release, feature parity will not be maintained. This modification is a rolling-release designed always for the newest game version.**